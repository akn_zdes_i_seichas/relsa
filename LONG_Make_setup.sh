#!/bin/bash
#
############################################################################
#.COPYRIGHT    (c)  2006  SAAO, CapeTown
#.IDENT        LONG_Make_setup.sh
#.LANGUAGE     SHyRAF  :-)
#.AUTHOR       AKN   (Kniazev A.Y.), SALT
#
#.KEYWORDS
#.PURPOSE      Creation of setup name
#
#.COMMENT
#.VERSION      $Date: 2019/06/22 15:33:02 $
#              $Revision: 1.15 $
############################################################################
#
. ./LONG_epar.sh
file=$1
std=$2
    #
    #...For RSS
    #
if [ "${System}" = "RSS" ]; then
	#
	#...Read of LAMP ID
	#
    LAMPID=`LONG_read_des.sh ${file} LAMPID|sed 's/ //g'`
	#
	#...Read of grating name
	#
    GRATING=`LONG_read_des.sh ${file} GRATING|sed 's/ //g'`
	#
	#...Read of grating angle
	#
    GRANGLE=`LONG_read_des.sh ${file} GRTILT|sed 's/ //g'|awk '{printf "%5.2f",$1}'`
	#
	#...Read of Articulation Station
	#
    ARANGLE=`LONG_read_des.sh ${file} CAMANG|sed 's/ //g'|awk '{printf "%5.2f",$1}'`
	#
	#...Read of Binning factor
	#
    BIN=`LONG_read_des.sh ${file} CCDSUM|sed "s/'//g"|sed 's/ //g'|awk '{printf "%1dx%1d\n", substr($1,1,1),substr($1,2,2)}'`
	#
	#...Read of Julian day
	#
    JD=`LONG_read_des.sh ${file} JD|sed "s/'//g"|awk '{printf "%13.5f",$1}'`
    if [ "z${JD}" = "z" ]; then
        JD=$(jday -d $(dfits ${file} |fitsort DATE-OBS TIME-OBS|tail -n +2|awk '{print $2,$3}'))
    fi
	#
	#...Read of GAIN and READ-OUT speed
	#
    GAINSET=`LONG_read_des.sh ${file} GAINSET|sed "s/'//g"`
    ROSPEED=`LONG_read_des.sh ${file} ROSPEED|sed "s/'//g"`
	#
	#...Forming of output
	#
    if [ "z${std}" = "zstd" ]; then
        echo -n "Std_${GRATING}_${ARANGLE}_${GRANGLE}_${BIN}_${JD}"|sed 's/ //g'
    elif [ "z${std}" = "zHRSstd" ]; then
        echo -n "Std"|sed 's/ //g'
    elif [ "z${std}" = "zflat" ]; then
        echo -n "FLAT_${GRATING}_${ARANGLE}_${GRANGLE}_${GAINSET}_${ROSPEED}_${BIN}_${JD}"|sed 's/ //g'
    elif [ "z${std}" = "zflatn" ]; then
        echo -n "FLATn_${GRATING}_${ARANGLE}_${GRANGLE}_${GAINSET}_${ROSPEED}_${BIN}_${JD}"|sed 's/ //g'
    elif [ "z${std}" = "zgain" ]; then
        echo -n "Gain_cor_${GAINSET}_${ROSPEED}_${BIN}_${JD}"|sed 's/ //g'
    elif [ "z${std}" = "zarc" ]; then
        echo -n "${LAMPID}_${GRATING}_${ARANGLE}_${GRANGLE}_${BIN}_${JD}"|sed 's/ //g'
    elif [ "z${std}" = "zobjshort" ]; then
        echo -n "NONE_${GRATING}_${ARANGLE}_${GRANGLE}_${BIN}_${JD}"|sed 's/ //g'
    else
        echo -n "${LAMPID}_${GRATING}_${ARANGLE}_${GRANGLE}_${BIN}_${JD}"|sed 's/ //g'
    fi
    #
    #...For SCORPIO
    #
elif [ "${System}" = "SCORPIO" ]; then
	#
	#...Read of Binning factor
	#
    BIN=`LONG_read_des.sh "${file}" BINNING|sed 's/ //g'|sed "s/'//g"`
	#
	#...Forming of output
	#
    if [ "${std}" = "std" ]; then
	echo -n "Std_VPHG550G_${BIN}"
    else
	echo -n "HeNeAr_VPHG550G_${BIN}"
    fi
    #
    #...For GSPEC
    #
elif [ "${System}" = "GSPEC" ]; then
	#
	#...Read of Binning factor
	#
    BIN="1x1"
	#
	#...Read of grating name
	#
    GRATING=${GRAT}
	#
	#...Read of grating angle
	#
    GRANGLE=${GRANG}
	#
	#...Forming of output
	#
    if [ "${std}" = "std" ]; then
	echo -n "Std_${GRATING}_${GRANGLE}_${BIN}"
    else
	#
	#...Read of LAMP ID
	#
	LAMPID=`LONG_read_des.sh ${file} ARC-LAMP|sed 's/ //g'`
	    #
	echo -n "${LAMPID}_${GRATING}_${GRANGLE}_${BIN}"
    fi
else
	#
	#...Forming of output for common case
	#
    echo -n "NONE_NONE_NONE_NONE_NONE"
fi
