#!/bin/bash
#
############################################################################
#.COPYRIGHT    (c)  2006  SAAO, CapeTown
#.IDENT        LONG_Make_date.sh
#.LANGUAGE     SHyRAF  :-)
#.AUTHOR       AKN (Kniazev A.Y.), SALT
#
#.KEYWORDS
#.PURPOSE      Creation of date in the format 20060606
#
#.COMMENT
#.VERSION      $Date: 2008/04/28 11:18:26 $
#              $Revision: 1.3 $
############################################################################
#
if [ ${System} = "RSS" ]; then
    ddate=`echo $1 | tr -d "[a-z]"|tr -d "[A-Z]"|tr -d '.'|sed 's/....$//'`
    echo ${ddate}
elif [ ${System} = "SCORPIO" ]; then
    name=`pwd`
    ddate=`basename ${name}`
    echo ${ddate}
elif [ ${System} = "GSPEC" ]; then
    ddate=`LONG_read_des.sh ${1} DATE-OBS|sed 's/-//g'`
    echo ${ddate}
fi
