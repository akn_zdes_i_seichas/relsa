#!/bin/bash
#
############################################################################
#.COPYRIGHT    (c)  2020  SAAO, CapeTown
#.IDENT        LONG_sens.sh
#.LANGUAGE     SHyRAF  :-)
#.AUTHOR       AKN   (Kniazev A.Y.), SALT
#
#.KEYWORDS     Long
#.PURPOSE      Script for the selection proper sensitivity curve by date
#
#.COMMENT      What script is doing:
#
#
#.VERSION      $Date: 2020/04/12 11:18:26 $
#              $Revision: 1.0 $
############################################################################
#
name=$1
best_file=$2
dir_sense=/home/DATA/RR_Lyres/Sample/SPST
cp -v /home/akniazev/bin/LONG_epar.sh ./
    #
    #...(1) Select the best day of sensetivity curve the closest day
    #
if [ "z${best_file}" == "z" ] ; then
    tmp_max=100000
    Date=`echo ${name} | cut -b 1-8`
    best_date=${Date}
    #
    for i in `ls ${dir_sense}/Std_*.fits`
    do
	date_tmp=`basename $i | cut -b 28-35`
	tmp=`expr ${Date} - ${date_tmp}`
	tmp=`echo ${tmp}|awk '{print sqrt($1**2)}'`
	if [ ${tmp} -le ${tmp_max} ] ; then
	    if [ ${tmp} -ge 0 ] ; then
		tmp_max=${tmp}
		best_date=${date_tmp}
		best_file=${i}
	    fi
	fi
    #    echo "${Date} - $date_tmp = $tmp"
    done
    echo "****************"
    echo "Pipeline selects file with sensitivity curve `basename ${best_file}`"
    echo "****************"
    cp -v ${best_file} ./sens.fits
else
    echo "****************"
    echo "Pipeline selects file from the input ${best_file}"
    echo "****************"
    cp -v ${dir_sense}/${best_file} ./sens.fits
fi
#
ls mbxgpP${name}eews.0001.fits >obj.lst
LONG_flux.sh obj=obj.lst sens_file=sens.fits
#
rm -rf uparm sens.fits *.par ./LONG_epar.sh
rm -rf FORGRdrs.KEY middummclear.prg login.cl
