#!/bin/bash
#
############################################################################
#.COPYRIGHT    (c)  2018  SAAO, CapeTown
#.IDENT        LONG_flat.sh
#.LANGUAGE     SHyRAF  :-)
#.AUTHOR       AKN   (Kniazev A.Y.), SALT
#
#.KEYWORDS     Long
#.PURPOSE      Script for creation of the flat
#
#.COMMENT      What script is doing:
#
#
#.VERSION      $Date: 2008/04/28 11:18:26 $
#              $Revision: 1.7 $
############################################################################
#
	 #
	 # ...Some initial setups
	 #
	    #...Global
	    #
    if [ -f ./LONG_epar.sh ]; then
	. ./LONG_epar.sh
    else
	cp -v ${HOME}/bin/LONG_epar.sh ./
	. LONG_epar.sh
    fi
	    #
	    #...Local
	    #
    eval $*
############################################################################
	#
	# ...make iraf input file
	#
    echo print "'***** Vsemy prishla jopa *****'"   >  $iraffile
    echo 'set imtype          = "fits"'             >> $iraffile
    echo images                                     >> $iraffile
    echo utilities                                  >> $iraffile
    echo noao                                       >> $iraffile
    echo imcombine input="*ee.fits" output="FLAT.fits" combine=median scale=mode >> $iraffile
	#
    echo logout                                     >> $iraffile
    if [ -f login.cl ]; then
	    mv login.cl login.cl.ST
    fi
    cl < ${iraffile}
    if [ -f login.cl.ST ]; then
	    mv login.cl.ST login.cl
    fi
	#
	#...Delete all temporary files
	#
    rm -rf ${iraffile}
############################################################################
