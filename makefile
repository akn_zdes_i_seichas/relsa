##########################################################################
# .COPYRIGHT:   Copyright (c) 2020 South African Astronomical Observatory
#						all rights reserved
# .TYPE		make file
# .NAME         makefile
# .LANGUAGE	makefile syntax
# .ENVIRONMENT  Unix Systems
# .COMMENT      Installation files for the Long-slit calculations
#
# .REMARKS	
# .AUTHOR       A.Y.Kniazev
##########################################################################
    #
    #...Setup
    #
LN = ln -sf
BINDIR  = ${HOME}/bin

all:
	@(for file in *.sh LONG_epar.sh.OBJECT *.csh; \
		do  $(LN) $(BINDIR)/LONG_slit/$$file $(BINDIR)/$$file; \
	done)

clean:
	@(OBJS=`ls *.sh LONG_epar.sh.OBJECT` ; cd $(BINDIR) ; rm -f $$OBJS)
	@(OBJS=`ls *.csh` ; cd $(BINDIR) ; rm -f $$OBJS)
