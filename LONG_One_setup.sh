#!/bin/bash
#
############################################################################
#.COPYRIGHT    (c)  2006  SAAO, CapeTown
#.IDENT        LONG_One_setup.sh
#.LANGUAGE     shMIDAF  :-) (shell+MIDAS+IRAF)
#.AUTHOR       AKN   (Kniazev A.Y.), SALT
#
#.KEYWORDS     Long
#.PURPOSE      Script for reduction of one setup of LONG-slit data
#
#.COMMENT      What script is doing:
#              1. Script checks that calibrations for this spectral setup
#                 exist:
#                 a. If one specified reference spectrum, script will
#                    check for identification and copy it.
#                 b. If no reference spectra was specified, the script
#                    will try to find 2d fit for this setup and copy it.
#              2. If reference spectrum was specified, reidentification
#                 and fitcoord will be re-done.
#              3. 2d transformation will be done on base either new
#                 or standard fit for all input files.
#              4. Background will be done for all input files.
#
#
#.VERSION      $Date: 2019/06/15 16:45:27 $
#              $Revision: 1.24 $
############################################################################
#
	 #
	 # ...Help
	 #
help() {
	echo "Usage: LONG_One_setup.sh obj=obj_list [arc=arc_list] [renew=no]"
	echo "where:"
	echo "       obj_list - list of objects for this arc"
	echo "       arc_list - list of arcs (currently - one)"
	echo "       renew    - start to identify this setup from the scratch [yes]"
	echo "                  or check our database and use it [no]"
	echo ""
}
############################################################################
 #
 # ...Some initial setups
 #
    #...Global
    #
if [ -f ./LONG_epar.sh ]; then
    . ./LONG_epar.sh
else
    echo " No LONG_epar.sh file in the current directory *****"
    exit 1
fi
    #
    #...Local
    #
eval $*
############################################################################
#
# ...Check input parameters
#
    #...For help
    #
if [ "z${help}" = "zyes" -o $# = 0 ]; then
    help
    exit 1
fi
    #
    # ...For list of objects
    #
if [ "z${obj}" = "z" -o ! -s "${obj}" ]; then
    echo ""
    echo "***** LONG_One_setup.sh: no objects were specified *****"
    echo ""
    help
    exit 1
fi
    #
    # ...For list of Arcs in case of renew=yes
    #
if [ "z${renew}" != "z" ]; then
    renew=`echo ${renew}|tr '[:upper:]' '[:lower:]'`
    if [ "${renew}" = "yes" ]; then
        if [ -s "z${arc}" = "z" -o ! -s "${arc}" ]; then
            echo ""
            echo "***** LONG_One_setup.sh: no arcs were specified for the new identification *****"
            echo ""
            help
            exit 1
        fi
    fi
fi
    #
    # ...Existence of input files
    #
for i in `cat ${obj} ${arc}`
do
    if [ ! -f "${i}" ]; then
        echo "***** ############################################# *****"
        echo "***** NO file "${i}" on the disk...                      "
        echo "***** ############################################# *****"
        exit 10
    fi
done
############################################################################
#
# ...Some initial setup for IRAF
#
if [ "z${IRAF}" = "zpyraf" ]; then
    echo "**** Will use pyraf with global login.cl ..."
else
    if [ ! -f login.cl ]; then
         mkiraf
    fi
fi
if [ ! -d uparm ]; then mkdir uparm; fi
if [ ! -d database ]; then mkdir database; fi
    #
if [ -f loginuser.cl ]; then rm -f loginuser.cl; fi
############################################################################
#
# ...Define our spectral setup and copy data for it
#
    #...flag is a keyword that characterises what we need to do:
    #...flag=0 - identify(a)+reidentify(a)+fitcoord(a)+transform(o)+backfround(o)
    #   flag=1 -             reidentify(a)+fitcoord(a)+transform(o)+backfround(o)
    #   flag=2 -                           fitcoord(a)+transform(o)+backfround(o)
    #   flag=3 -                                       transform(o)+backfround(o)
    #   flag=4 -                                                    backfround(o)
    #   flag=5 -                                                    none
    #
flag=0
if [ "z${renew}" = "zyes" ]; then
    LONG_Make_ident.sh arc=${arc}
    flag=3
    if [ "z${arc}" = "z" ]; then arc=qqq.lst; fi
    echo ${closest}|tr -d '^fc' >${arc}1
fi
    #
    #...In case of input only object we need to find calibration
    #   for this setup and copy it to the current directory
    #
if [ "z${arc}" = "z" -o ! -s "${arc}" ]; then
    echo "***** ############################################# *****"
    echo "***** I have NO list of ARCs as input                    "
    echo "***** ############################################# *****"
    reference="`cat ${obj}|head -1|sed 's/.fits//g'`"
    setup=`LONG_Make_setup.sh ${reference}.fits objshort`
    closest=`LONG_Find_nearest.sh ${setup} objshort`
    if [ "z${closest}" != "zNONE" ]; then
        cp ${St_database}/${System}/${closest} ./database/${closest}.ST
        close=`echo ${closest}|tr -d '^fc'`
        ARCNAME="begin   ${close}"
        cat ./database/${closest}.ST|awk "{if(/begin/)print \"${ARCNAME}\";else print \$0}">./database/${closest}
        echo ""
        echo "***** I was need to copy ${closest} setup from the database *****"
        echo ""
    else
        echo "***** $0: I need to exit, because there is no identification *****"
        echo "***** $0: for the requested ${setup} setup in our database  *****"
        exit 1
    fi
    if [ "z${arc}" = "z" ]; then arc=qqq.lst; fi
    echo ${closest}|tr -d '^fc' >${arc}1
    flag=3
else
        #...In case of reference we need to find identification
        #   for this setup and copy it to the current directory
        #
    echo "***** ############################################# *****"
    echo "***** We have the list for ARC as input                  "
    echo "***** ############################################# *****"
    reference="`cat ${arc}|head -1|sed 's/.fits//g'`"
    setup=`LONG_Make_setup.sh ${reference}.fits arc`
        #
        #...With new idea I will copy to the local database ONLY
        #   delected file from the identification database
        #
    closest=`LONG_Find_nearest.sh ${setup} obj`
    if [ "z${closest}" != "zNONE" ]; then
        NAXIS2=`dfits ${reference}.fits|fgrep NAXIS2|awk '{print int($3/2)+1}'`
        ARCNAME="begin   identify ${reference}[*,${NAXIS2}]"
         ID="        id      ${reference}"
          IMAGE="        image   ${reference}[*,${NAXIS2}]"
        cp ${St_database}/${System}/${closest} ./database
        cat ./database/${closest}|awk "{if(/begin/)print \"${ARCNAME}\";else print \$0}"|\
        awk "{if(/id[[:blank:]]/)print \"${ID}\";else print \$0}"|\
        awk "{if(/image/)print \"${IMAGE}\";else print \$0}"\
        >./database/id${reference}
        flag=1
    else
        echo "There is no identification for ${reference} setup in our database"
        echo "***** ######################################################## *****"
        echo "***** Sorry, but I HAVE NO such spectral setup reduced earlier *****"
        echo "***** ######################################################## *****"
        flag=0
        LONG_Make_ident.sh arc=${arc}
        if [ $? != 0 ] ; then
        echo "***** $0: Something is wrong *****"
        exit 1
        fi
        if [ "z${arc}" = "z" ]; then arc=qqq.lst; fi
        echo ${closest}|tr -d '^fc' >${arc}1
        flag=3
    fi
fi
#
# ...Define our reference spectrum and prepare
#    this table
#
if [ ${flag} -le 1 ]; then
    coordlist=`echo ${setup}|tr "_" " "|awk '{print $1}'|tr '[:upper:]' '[:lower:]'`
    if [ -f ${St_red_dir}/linelists/${coordlist}.dat ]; then
        cp ${St_red_dir}/linelists/${coordlist}.dat ./
    else
        echo "***** $0: There is no table with lines for this ARC = ${coordlist}.dat *****"
        exit 1
    fi
fi
#
# ...Make our lists and delete existed files
#
sed 's/.fits/w.fits/g'   <${obj} >${obj}${wcalib}
sed 's/.fits/ws.fits/g'  <${obj} >${obj}${wcalib}${backgr}
if [ $flag -le 2 ]; then
sed 's/.fits//g' <${arc} >${arc}1
ref_spec=`cat ${arc}1|tr -d '\r'`
fi
if [ $flag -le 3 ]; then
rm -rf `cat ${obj}${wcalib} ${obj}${wcalib}${backgr}`
fi
if [ $flag -eq 4 ]; then
rm -rf `cat ${obj}${wcalib}${backgr}`
fi
#
# ...make iraf input file
#
echo print "'***** LONG_One_setup *****'"       >  $iraffile
echo 'set imtype          = "fits"'             >> $iraffile
#    echo reset stdimage        = imt6               >> $iraffile
#    echo images                                     >> $iraffile
#    echo plot                                       >> $iraffile
#    echo dataio                                     >> $iraffile
#    echo lists                                      >> $iraffile
#    echo tv                                         >> $iraffile
#    echo utilities                                  >> $iraffile
echo noao                                       >> $iraffile
echo twodspec                                   >> $iraffile
echo longslit                                   >> $iraffile
echo print "'***** Reducing of ARCs *****'"     >> $iraffile
    #
    #   only in case flag <= 1
    #
if [ $flag -le 1 ]; then
    echo reidentify reference=${reference} images=${ref_spec} interactive=${interactive} newaps=${newaps} override=${override} refit=${refit} nlost=${nlost} coordlist=${coordlist}.dat verbose=${verbose} trace=${trace} section=\'${section}\' shift=${shift} search=${search} addfeatures=${addfeatures} step=${step} nsum=${nsum} >> $iraffile
    echo ! LONG_idCorrect.sh database/id${reference} >> $iraffile
fi
    #
    #...Check that this two-dimensional solution exists for this
    #   reference spectrum.
    #
if [ $flag -le 2 ]; then
    echo fitcoord   images=\@${arc}1 interactive=${interactive_fit} combine=${combine} functio=${functio} xorder=${xorder} yorder=${yorder} >> $iraffile
fi
if [ $flag -le 3 ]; then
    echo print "'***** Reducing of objects ***** '" >> $iraffile
    echo hedit images=\@${obj} fields=DISPAXIS value=1 add=yes addonly=yes ver-  >> $iraffile
    echo transform input=\@${obj} output=\@${obj}${wcalib} fitnames=\@${arc}1 interptype=${interptype} flux=${flux} blank=${blank} x1=${x1} x2=${x2} dx=${dx} y1=${y1} y2=${y2} dy=${dy} >> $iraffile
    if [ -s mask.lst ]; then
        echo print "'***** Start wavelength transformation for masks *****'"     >> $iraffile
        echo hedit images=\@mask.lst fields=DISPAXIS value=1 add=yes addonly=yes ver-  >> $iraffile
        echo transform input=\@mask.lst output=\@maskw.lst fitnames=\@${arc}1 interptype=${interptype} flux=${flux} blank=${blank} x1=${x1} x2=${x2} dx=${dx} y1=${y1} y2=${y2} dy=${dy} >> $iraffile
    fi
    echo print "'***** Start background calculation *****'"     >> $iraffile
    echo background input=\@${obj}${wcalib} output=\@${obj}${wcalib}${backgr} axis=${axis} interactive=${interactive_back} naverage=${naverage} function=${function} order=${order} low_rej=${low_rej} high_rej=${high_rej} niterate=${niterate} grow=${grow} >> $iraffile
fi
#    echo logout                                     >> $iraffile
#
#...Execution of IRAF depending on request for interactive work
#   with "fitcoord" task or/and "background" task
#
if [ "z${IRAF}" = "zpyraf" ]; then
#        echo '! kill -9 `ps aux|fgrep ${USER}|fgrep python|fgrep -v fgrep|awk "{print \$2}"`'    >> $iraffile
    mv ${iraffile} loginuser.cl
    iraffile="loginuser.cl"
    pyraf -n
else
    if [ ${interactive_fit} = "no" -a ${interactive_back} = "no" -o $flag -ge 3 ]; then
        mv ${iraffile} loginuser.cl
        cl # < ${iraffile}
    fi
fi
#
#...Finally, we need to copy the final fd file with 2D solution into our database.
#   We will need to use it for the closest spectrophotometric standard with the same
#   setup but only in case we make it now, but not used previous (flag>=3)
#
if [ $flag -le 2 ]; then
    cp ./database/fc${reference} ${St_database}/${System}/fc${setup}
fi
#
#...Delete all temporary files
#
rm -f ${obj}${wcalib} ${obj}${wcalib}${backgr} plotfile ${arc}1 $$
rm -f ${iraffile} ${coordlist}.dat
############################################################################
