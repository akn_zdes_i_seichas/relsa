#!/bin/bash
#
############################################################################
#.COPYRIGHT    (c)  2006  SAAO, CapeTown
#.IDENT        LONG_init_wav.sh
#.LANGUAGE     SHyRAF  :-)
#.AUTHOR       AKN (Kniazev A.Y.), SALT
#
#.KEYWORDS
#.PURPOSE      Calculate of initial wavelength and step for current setup
#
#.COMMENT
#.VERSION      $Date: 2006/12/07 14:04:42 $
#              $Revision: 1.6 $
############################################################################
#
if [ -f ./LONG_epar.sh ]; then
    . ./LONG_epar.sh
else
    . LONG_epar.sh
fi
file=$1
param=$2
############################################################################
# 	#
# 	# ...Some initial setups for IRAF
# 	#
#     if [ ! -f login.cl ]; then
# 	LONG_mkiraf.csh
#     fi
# 	    #
#     if [ -f loginuser.cl ]; then rm loginuser.cl; fi
############################################################################
	#
	#...Main body
	#
if [ ${System} = "RSS" ]; then
    if [ -f ${file} ]; then
	grat=`LONG_read_des.sh ${file} GRATING|sed 's/ //g'|sed 's/PG//g'`
	grangle=`LONG_read_des.sh ${file} GR-ANGLE|sed 's/ //g'|awk '{printf "%7.3f",$1}'`
	arangle=`LONG_read_des.sh ${file} AR-ANGLE|sed 's/ //g'|awk '{printf "%10.6f",$1}'`
	start=`echo "1"|awk "{printf \"%8.3f\", (1e7/${grat})*(sin(0.017453292*${grangle})+sin(0.017453292*(${arangle}-${grangle}-8.3)))}"`
#   blue_end=`echo "1"|awk "{printf \"%8.3f\", (1e7/${grat})*(sin(0.017453292*${grangle})+sin(0.017453292*(${arangle}-${grangle}-2.941)))}"`
	blue_end=`echo "1"|awk "{printf \"%8.3f\", (1e7/${grat})*(sin(0.017453292*${grangle})+sin(0.017453292*(${arangle}-${grangle}-3.0)))}"`
	cent_beg=`echo "1"|awk "{printf \"%8.3f\", (1e7/${grat})*(sin(0.017453292*${grangle})+sin(0.017453292*(${arangle}-${grangle}-2.679)))}"`
	cent_end=`echo "1"|awk "{printf \"%8.3f\", (1e7/${grat})*(sin(0.017453292*${grangle})+sin(0.017453292*(${arangle}-${grangle}+2.54)))}"`
	red_beg=`echo "1"|awk "{printf \"%8.3f\", (1e7/${grat})*(sin(0.017453292*${grangle})+sin(0.017453292*(${arangle}-${grangle}+2.941)))}"`
	end=`echo "1"|awk "{printf \"%8.3f\", (1e7/${grat})*(sin(0.017453292*${grangle})+sin(0.017453292*(${arangle}-${grangle}+8.15)))}"`
	naxis=`LONG_read_des.sh ${file} NAXIS1|awk '{print $1}'`
	step=`echo "1"|awk "{printf \"%8.5f\", (${end}-${start})/${naxis}}"`
	echo "${start} ${step} ${blue_end} ${cent_beg} ${cent_end} ${red_beg}"
    #
	if [ "${param}" = "yes" ]; then
	    echo print "'***** Vsemy prishla jopa *****'"   >  $iraffile
	    echo 'set imtype          = "fits"'             >> $iraffile
# 	    echo reset stdimage        = imt6               >> $iraffile
# 	    echo images                                     >> $iraffile
# 	    echo plot                                       >> $iraffile
# 	    echo dataio                                     >> $iraffile
# 	    echo lists                                      >> $iraffile
# 	    echo tv                                         >> $iraffile
# 	    echo utilities                                  >> $iraffile
	    echo noao                                       >> $iraffile
	    echo twodspec                                   >> $iraffile
#            echo hedit images=${file}[0] fields=CRVAL1 value=${start} add=yes addonly=yes ver-  >> $iraffile
#            echo hedit images=${file}[0] fields=CRPIX1 value=1 add=yes addonly=yes ver-  >> $iraffile
#            echo hedit images=${file}[0] fields=CDELT1 value=${step} add=yes addonly=yes ver-  >> $iraffile
#            echo hedit images=${file}[0] fields=CD1_1 value=${step} add=yes addonly=yes ver-  >> $iraffile
	    echo hedit images=${file} fields=CRVAL1 value=${start} add=yes addonly=yes ver-  >> $iraffile
	    echo hedit images=${file} fields=CRPIX1 value=1 add=yes addonly=yes ver-  >> $iraffile
	    echo hedit images=${file} fields=CDELT1 value=${step} add=yes addonly=yes ver-  >> $iraffile
	    echo hedit images=${file} fields=CD1_1 value=${step} add=yes addonly=yes ver-  >> $iraffile
	    echo logout                                     >> $iraffile
	    if [ -f login.cl ]; then
		    mv login.cl login.cl.ST
	    fi
	    cl < ${iraffile}
	    if [ -f login.cl.ST ]; then
		    mv login.cl.ST login.cl
	    fi
	    rm -f ${iraffile}
	fi
	    #
    else
	echo "***** LONG_init_wav.sh: No file ${file} *****"
	exit 1
    fi
else
    echo -n "OK"
fi
############################################################################
