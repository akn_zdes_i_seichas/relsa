#!/bin/sh
#
############################################################################
#.COPYRIGHT    (c)  2006  SAAO, CapeTown
#.IDENT        LONG_epar.sh
#.LANGUAGE     SHyRAF  :-)
#.AUTHOR       AKN   (Kniazev A.Y.), SALT
#
#.KEYWORDS
#.PURPOSE      Global definition of all parameteres for the package
#
#.COMMENT
#.VERSION      $Date: 2019/07/02 20:38:56 $
#              $Revision: 1.1 $
############################################################################
#
    #
    #...Database location
    #
#St_red_dir="/home/akniazev/SALT/Data/Reduction/ST_red"
St_red_dir="/home/akniazev/ST_red"
    #
    #...Name of the IRAF temporary file
    #
iraffile="Iraftemp"
############################################################################
    #
    #...For subroutine of reading information form FITS headers:
    #   Type of work with FITS-files: IRAF | ftool | MIDAS | dfits
    #
#FITS_work=IRAF
FITS_work=dfits
    #
    #...For "FITS_work=IRAF" you need to know IRAF locaion
    #
irafdir=/iraf/iraf/bin.linux
############################################################################
    #
    #...Observational systems for which these scripts are working:
    #   1. RSS - prime focus spectrograph of SALT
    #   2. SCORPIO - prime focus spectrograph of 6m
    #   3. GSPEC - Grating Spectrograph with SITE CCD at SAAO 1.9m
    #
	#
export System=RSS
Observatory=saao
	#
#export System=SCORPIO
#Observatory=sao
	#
#export System=GSPEC
#Observatory=saao
#GRAT=G3
#GRANG=7.1
############################################################################
    #
    #...Global definitions of reduction steps
    #
cosmic=""
subtract=""
wcalib="w"
backgr="s"
calibflux="f"
############################################################################
    #
    #...Keys for specific programs
    #
	#...For "LONG_Std_red.sh"
	#
sens_app=no     #...append new star ('yes') to the file with standard stars
		#   or create new ('no') file
sens=yes        #...make or not sensitivity curve [yes|no]
copy=yes        #...copy or not the resulted sensitivity curve into
		#   database [yes|no]
############################################################################
#
#    WARNING!!! parameters "parameter=tmp" ARE CHANGING inside of the scripts
#               and CAN NOT be controled from here
#    They are just listed below to show their existence
#
############################################################################
    #
    #...Parameters for IRAF tasks
    #
	#...longslit.identify.
	#   For "LONG_Make_ident.sh" script
	#
    images=tmp
    coordlist_id=tmp
    function_id=chebyshev
    order_id=4
    fwidth=6.
    cradius=6.
	#
	#...longslit.reidentify.
	#   For "LONG_Make_ident.sh" and "LONG_One_setup.sh" scripts.
	#
    reference=tmp
    images=tmp
    coordlist=tmp
    interactive=no
    newaps=yes
    override=no
    refit=yes
    nlost=20
    verbose=yes
    trace=yes
    section='middle line'
    shift=INDEF
    search=INDEF
    addfeatures=no
	#
	#...longslit.fitcoord
	#   For "LONG_Make_ident.sh" and "LONG_One_setup.sh" scripts.
	#
    images=tmp
    interactive_fit=no
    combine=no
    functio=legendre
    xorder=5
    yorder=3
	#
	#...longslit.transform
	#   For "LONG_One_setup.sh" script.
	#
    input=tmp
    output=tmp
    fitnames=tmp
    interptype=linear
    flux=yes
    blank=INDEF
    x1=INDEF
    x2=INDEF
    dx=INDEF
    y1=INDEF
    y2=INDEF
    dy=INDEF
	#
	#...longslit.background
	#   For "LONG_One_setup.sh" script.
	#
    input=tmp
    output=tmp
    axis=2
    interactive_back=no
    naverage=1
    function=chebyshev
    order=5
    low_rej=2.
    high_rej=1.5
    niterate=5
    grow=0.
	#
	#...apextract.apall
	#   For "LONG_Std_red.sh" script.
	#
    input=tmp
    output=tmp
    format=onedspec
    interactive_apall=yes
    nfind=1
    llimit=-25
    ulimit=25
    t_order=6
    ylevel=INDEF
    line=INDEF
	#
	#...longslit.standard
	#   For "LONG_Std_red.sh" script.
	#
    input=tmp
    output=tmp
    caldir=${St_red_dir}/caldir/
    interact_st=no
    star_name=tmp
    extinct=${St_red_dir}/caldir/suzextinct2.dat
#    extinct=${St_red_dir}/caldir/ctioextinct.dat
	#
	#...longslit.sensfunc
	#   For "LONG_Std_red.sh" script.
	#
    standard=tmp
    sensitiv=tmp
    interactive_sens=yes
	#
	#...longslit.calibrate
	#   For "LONG_Std_red.sh" and "LONG_flux.sh" scripts.
	#
    input=tmp
    output=tmp
    sensitivity_cal=tmp
