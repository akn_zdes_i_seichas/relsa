#!/bin/bash
#
############################################################################
#.COPYRIGHT    (c)  2006  SAAO, CapeTown
#.IDENT        LONG_Make_ident.sh
#.LANGUAGE     SHyRAF  :-)
#.AUTHOR       AKN (Kniazev A.Y.), SALT
#
#.KEYWORDS     Long
#.PURPOSE      Script making idendification of one setup of LONG-slit data
#
#.COMMENT      What script is doing:
#
#.VERSION      $Date: 2006/12/07 14:02:13 $
#              $Revision: 1.5 $
############################################################################
#
	 #
	 # ...Help
	 #
help() {
	echo "Usage: LONG_Make_ident.sh arc=arc_list renew=yes"
	echo "where:"
	echo "       arc_list - List with reference spectrum for this setup"
	echo "       renew    - start to identify this setup from the scratch [yes]"
	echo "                  or check our database and use it [no]"
	echo "      "
	echo 'Program suggests that input file has .fits extension'
	echo "      "
}
############################################################################
	 #
	 # ...Some initial setups
	 #
	    #...Global
	    #
    if [ -f ./LONG_epar.sh ]; then
	. ./LONG_epar.sh
    else
	. LONG_epar.sh
    fi
	    #
	    #...Local
	    #
    eval $*
############################################################################
	#
	# ...Check input parameters
	#
	    #...For help
	    #
    if [ "z${help}" = "zyes" -o $# = 0 ]; then
	help
	exit 1
    fi
	    #
	    # ...For list of objects
	    #
    if [ "z${arc}" = "z" -o ! -s "${arc}" ]; then
	echo ""
	echo "***** LONG_Make_ident.sh: List with Arc data was not specified or empty *****"
	echo ""
	help
	exit 1
    fi
	#
	# ...Existence of input files
	#
    for i in `cat ${arc}`
    do
	if [ ! -f "${i}" ]; then
	    echo "***** ############################################# *****"
	    echo "***** NO Arc file "${i}" in the current directory        "
	    echo "***** ############################################# *****"
	    exit 10
	fi
    done
############################################################################
	#
	# ...Some initial setup for IRAF
	#
if [ "z${IRAF}" = "zpyraf" ]; then
    echo "**** Will use pyraf with global login.cl ..."
else
    if [ ! -f login.cl ]; then
         mkiraf
    fi
fi
if [ ! -d uparm ]; then mkdir uparm; fi
if [ ! -d database ]; then mkdir database; fi
    #
if [ -f loginuser.cl ]; then rm -f loginuser.cl; fi
############################################################################
	#
	# ...Define our spectral setup
	#
    spec=`cat ${arc}`
    reference=`LONG_Make_setup.sh ${spec} arc`
    if [ `echo ${reference}|fgrep NONE` ]; then
	echo "***** LONG_Make_ident.sh: ${reference} *****"
	echo "***** LONG_Make_ident.sh: Can not get spectral setup for this system *****"
	exit 1
    else
	cp ${spec} ${reference}.fits
	rm -f ./database/*${reference}
    fi
	#
	# ...And calculate initial wavelength and step for it
	#    (if we can do it, of course)
	#
    LONG_init_wav.sh ${reference}.fits yes
    if [ $? != 0 ] ; then
	 echo "***** LONG_Make_ident.sh: Something wrong *****"
	 exit 1
    fi
	#
	# ...Define our reference spectrum and prepare this table
	#
    coordlist=`echo ${reference}|tr "_" " "|awk '{print $1}'|tr '[:upper:]' '[:lower:]'`
    if [ -f ${St_red_dir}/linelists/${coordlist}.dat ]; then
	cp ${St_red_dir}/linelists/${coordlist}.dat ./
    else
	echo "***** LONG_Make_ident.sh: There is no table with coordinate list in our directory *****"
	exit 1
    fi
	#
	# ...make iraf input file
	#
    echo print "'***** Vsemy prishla jopa *****'"   >  $iraffile
    echo 'set imtype          = "fits"'             >> $iraffile
    echo reset stdimage        = imt6               >> $iraffile
    echo images                                     >> $iraffile
    echo plot                                       >> $iraffile
    echo dataio                                     >> $iraffile
    echo lists                                      >> $iraffile
    echo tv                                         >> $iraffile
    echo utilities                                  >> $iraffile
    echo noao                                       >> $iraffile
    echo twodspec                                   >> $iraffile
    echo longslit                                   >> $iraffile
    if [ "${System}" = "RSS" ]; then
#        echo identify images=${reference}[1] coordlist=${coordlist}.dat function=${function_id} order=${order_id} fwidth=${fwidth} cradius=${cradius} >> $iraffile
#        echo reidentify reference=${reference}[1] images=${reference} interactive=${interactive} newaps=${newaps} override=${override} refit=${refit} nlost=${nlost} coordlist=${coordlist}.dat verbose=${verbose} >> $iraffile
#        echo fitcoord images=${reference}[1] interactive=yes combine=${combine} functio=${functio} xorder=${xorder} yorder=${yorder} >> $iraffile
	echo identify images=${reference} coordlist=${coordlist}.dat function=${function_id} order=${order_id} fwidth=${fwidth} cradius=${cradius} section=\'${section}\' >> $iraffile
	echo !cp database/id${reference} database/id${reference}_ONELINE >> $iraffile
	echo reidentify reference=${reference} images=${reference} interactive=${interactive} newaps=${newaps} override=${override} refit=${refit} nlost=${nlost} coordlist=${coordlist}.dat verbose=${verbose} section=\'${section}\' >> $iraffile
	echo fitcoord images=${reference} interactive=yes combine=${combine} functio=${functio} xorder=${xorder} yorder=${yorder} >> $iraffile
    else
	echo identify images=${reference} coordlist=${coordlist}.dat function=${function_id} order=${order_id} fwidth=${fwidth} cradius=${cradius} >> $iraffile
	echo reidentify reference=${reference} images=${reference} interactive=${interactive} newaps=${newaps} override=${override} refit=${refit} nlost=${nlost} coordlist=${coordlist}.dat verbose=${verbose} >> $iraffile
	echo fitcoord images=${reference} interactive=yes combine=${combine} functio=${functio} xorder=${xorder} yorder=${yorder} >> $iraffile
    fi
    echo logout                                     >> $iraffile
    echo '! kill -9 `ps aux|fgrep ${USER}|fgrep python|fgrep -v fgrep|awk "{print \$2}"`'    >> $iraffile
	#
	#...Execution of IRAF depending on request for interactive work
	#   with "fitcoord" task or/and "background" task
	#
    mv ${iraffile} loginuser.cl
    iraffile="loginuser.cl"
#    mv loginuser.cl AAA.cl
#    pyraf -n <AAA.cl
    pyraf -n
	#
	#...Copy all output files into Database directory
	#
    cp -v database/id${reference}_ONELINE ${St_database}/${System}/id${reference}
    cp -v database/fc${reference} ${St_database}/${System}/
	#
	#...Delete all temporary files
	#
    rm -f ${iraffile} ${coordlist}.dat ${reference}.fits
############################################################################
