#!/bin/bash
#
############################################################################
#.COPYRIGHT    (c)  2006  SAAO, CapeTown
#.IDENT        LONG_read_des.sh
#.LANGUAGE     SHyRAF  :-)
#.AUTHOR       AKN (Kniazev A.Y.), SALT
#
#.KEYWORDS
#.PURPOSE      Read_of_descriptor from FITS file
#
#.COMMENT
#.VERSION      $Date: 2019/06/22 15:33:46 $
#              $Revision: 1.12 $
############################################################################
    #
    # ...Some initial setups
    #
       #...Global
       #
    if [ -f ./LONG_epar.sh ]; then
	. ./LONG_epar.sh
    else
	. LONG_epar.sh
    fi
	#
    file=$1
    des=$2
    if [ -f "${file}" ]; then
	if [ "z${des}" != "z" ]; then
		#
		#...For MIDAS
		#
	    if [ "${FITS_work}" = "MIDAS" ]; then
		if [ "${des}" = "OBJECT" ]; then des="IDENT" ; fi
		if [ "${des}" = "NAXIS1" ]; then des='NPIX' ; fi
		    if [ -f ${HOME}/midwork/login.prg ]; then
			mv ${HOME}/midwork/login.prg ${HOME}/midwork/login.prg$$
			echo -n `inmidas xh -p -j "read/des ${file} ${des} H; bye"|tr -d '\r'`
			mv ${HOME}/midwork/login.prg$$ ${HOME}/midwork/login.prg
		    else
			echo -n `inmidas xh -p -j "read/des ${file} ${des} H; bye"|tr -d '\r'`
		    fi
	    elif [ "${FITS_work}" = "ftool" ]; then
		#
		#...For ftool
		#
		export HEADAS='/usr/local/bin'
		ftool='/usr/local/bin/headas-init.sh'
		. $ftool
		fkeypar $file+0 ${des}
#                echo -n `pget fkeypar value | sed 's/ //g' | sed "s/'//g" `
		echo -n `pget fkeypar value | sed "s/'//g" `
		#
		#...For dfits
		#
	    elif [ "${FITS_work}" = "dfits" ]; then
		#
		dfits ${file}|grep ${des}|awk -F' = ' '{print $2}'|awk -F' / ' '{print $1}'|sed "s/'//g"
		#
		#...For IRAF
		#
	    elif [ "${FITS_work}" = "IRAF" ]; then
		    #
		    # ...make iraf input file
		    #
		echo images                                     > qqqq
		if [ "${des}" = "NAXIS1" ]; then
#                    echo hedit images=${file}[1] fields=${des} value="." add="no" addonly="no" update="yes" verify="no" show="no" delete="no" \>qqqq.Tmp >> qqqq
		    echo hedit images=${file} fields=${des} value="." add="no" addonly="no" update="yes" verify="no" show="no" delete="no" \>qqqq.Tmp >> qqqq
		else
#                    echo hedit images=${file}[0] fields=${des} value="." add="no" addonly="no" update="yes" verify="no" show="no" delete="no" \>qqqq.Tmp >> qqqq
		    echo hedit images=${file} fields=${des} value="." add="no" addonly="no" update="yes" verify="no" show="no" delete="no" \>qqqq.Tmp >> qqqq
		fi
		echo logout                                     >> qqqq
		if [ -f loginuser.cl ]; then
			mv loginuser.cl loginuser.cl.ST
		fi 
		if [ -f login.cl ]; then
			mv login.cl login.cl.ST
		fi 
		cl < qqqq >/dev/null
		awk -F' = ' '{print $2}' qqqq.Tmp |sed 's/"//g'
		rm -f qqqq qqqq.Tmp
		if [ -f loginuser.cl.ST ]; then
			mv loginuser.cl.ST loginuser.cl
		fi
		if [ -f login.cl.ST ]; then
			mv login.cl.ST login.cl
		fi 
	    else
		echo -n "-1"
	    fi
	else
	    echo -n "-10"
	fi
    else
	echo -n "-100"
    fi
