#!/bin/bash
#
############################################################################
#.COPYRIGHT    (c)  2006  SAAO, CapeTown
#.IDENT        LONG_flux.sh
#.LANGUAGE     SHyRAF  :-)
#.AUTHOR       AKN   (Kniazev A.Y.), SALT
#
#.KEYWORDS     Long
#.PURPOSE      Script for correction for sensitivity curve
#
#.COMMENT      What script is doing:
#
#
#.VERSION      $Date: 2008/04/28 11:18:26 $
#              $Revision: 1.7 $
############################################################################
#
	 #
	 # ...Help
	 #
help() {
	echo 'Usage: LONG_flux.sh obj=obj_list [sens_file=sensitivity]'
	echo "where:"
	echo "       obj_list    - list of objects"
	echo '       sensitivity - use this sensitivity file instead of standard one'
	echo "      "
	echo "Program suggests: "
	echo "1. All objects were 2D-linearized and background was subtracted."
	echo '2. Each file in the input list has .fits extension'
	echo "      "
}
############################################################################
	 #
	 # ...Some initial setups
	 #
	    #...Global
	    #
    if [ -f ./LONG_epar.sh ]; then
	. ./LONG_epar.sh
    else
	. LONG_epar.sh
    fi
	    #
	    #...Local
	    #
    eval $*
############################################################################
	#
	# ...Check of input parameters
	#
	    #...For help
	    #
    if [ "z${help}" = "zyes" -o $# = 0 ]; then
	help
	exit 1
    fi
	    #
	    # ...For list of objects
	    #
    if [ "z${obj}" = "z" -o ! -s "${obj}" ]; then
	echo ""
	echo "***** LONG_flux.sh: no objects were specified *****"
	echo ""
	help
	exit 1
    fi
	    #
	    # ...Existence of input files
	    #
    for i in `cat ${obj}` ${sens_file}
    do
	if [ ! -f "${i}" ]; then
	    echo "***** ############################################# *****"
	    echo "***** NO file "${i}" on the disk...                 *****"
	    echo "***** ############################################# *****"
	    exit 10
	fi
    done
############################################################################
#
# ...Some initial setup for IRAF
#
if [ "z${IRAF}" = "zpyraf" ]; then
    echo "**** Will use pyraf with global login.cl ..."
else
    if [ ! -f login.cl ]; then
        mkiraf
    fi
fi
    #
if [ -f loginuser.cl ]; then rm -f loginuser.cl; fi
############################################################################
	#
	# ...Create input name for sensitivity curve
	#
    if [ "z${sens_file}" = "z" ]; then
	file=`cat ${obj}|head -1`
	std_out=`LONG_Make_setup.sh ${file} std`
	ddate=`LONG_Make_date.sh ${file}`
	if [ ! -f ${std_out} ]; then
	    if [ -f ${St_red_dir}/${std_out} ]; then
		cp -v ${St_red_dir}/${std_out} ./
		echo "***** We use ${std_out} *****"
		echo
	    else
		echo "***** LONG_flux: There is no sensitivity curve for this setup *****"
		echo "***** LONG_flux: Your setup is ${std_out} *****"
		echo
		exit 1
	    fi
	else
	    echo "***** The sensitivity curve exists for this setup *****"
	    echo "***** Your setup is ${std_out} *****"
	    echo
	fi
    else
	std_out="${sens_file}"
	if [ ! -f ./${std_out} ]; then
	    if [ -f ${St_red_dir}/${std_out} ]; then
		cp -v ${St_red_dir}/${std_out} ./
		echo "***** We use ${std_out} *****"
		echo
	    else
		echo "***** The specified sensitivity curve does not exists *****"
		echo
		exit 1
	    fi
	fi
    fi
	#
	# ...make iraf input file
	#
    echo print "'***** Long_flux *****'"            >  $iraffile
    echo 'set imtype          = "fits"'             >> $iraffile
#     echo reset stdimage        = imt6               >> $iraffile
#     echo images                                     >> $iraffile
#     echo plot                                       >> $iraffile
#     echo dataio                                     >> $iraffile
#     echo lists                                      >> $iraffile
#     echo tv                                         >> $iraffile
#     echo utilities                                  >> $iraffile
    echo noao                                       >> $iraffile
    echo twodspec                                   >> $iraffile
    echo longslit                                   >> $iraffile
	#
	# ...Main loop
	#
    for i in `cat ${obj}`
    do
	aa=`echo ${i}|sed 's/.fits/'${calibflux}'.fits/g'`
	echo imdel ${aa}                            >> $iraffile
	LONG_Make_airm.sh ${i} ${i} ${Observatory} $iraffile
	if [ $? != 0 ] ; then
	    exit 1
	fi
	echo calibrate input=${i} output=${aa} sensitivity=${std_out} extinction=${extinct} >> $iraffile
    done
	#
    echo logout                                     >> $iraffile
    if [ -f login.cl ]; then
	    mv login.cl login.cl.ST
    fi
    cl < ${iraffile}
    if [ -f login.cl.ST ]; then
	    mv login.cl.ST login.cl
    fi
	#
	#...Delete all temporary files
	#
    rm -rf ${iraffile}
############################################################################
