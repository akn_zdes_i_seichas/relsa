#!/bin/bash
#
############################################################################
#.COPYRIGHT    (c) 2006 SAAO, CapeTown
#.IDENT        LONG_RSS_red.sh
#.LANGUAGE     shMIDAF  :-) (shell+MIDAS+IRAF)
#.AUTHOR       AKN (Kniazev A.Y.), SALT
#
#.KEYWORDS
#.PURPOSE      Script for reduction of LONG-slit RSS data
#
#.COMMENT
#.VERSION      $Date: 2008/04/28 11:18:26 $
#              $Revision: 1.15 $
############################################################################
#
	 #
	 # ...Help
	 #
help() {
	echo "Usage: LONG_red.sh obj=obj_list [arc=obj_arc] [[std=std_list] [std_arc=std_arc_list]]"
	echo "where:"
	echo "       obj_list - list of objects (without of standards)"
	echo "       obj_arc -  list of arcs for object (currently - one)"
	echo "       std_list - list of standards"
	echo "       std_arc  - list of arcs for standards (currently - one)"
	echo ""
}
######################################################################
#
# ...Some initial setups
#
    #...Global
    #
if [ -f ./LONG_epar.sh ]; then
    . ./LONG_epar.sh
else
    echo " No LONG_epar.sh file in the current directory *****"
    exit 1
fi
    #
    #...Local
    #
eval $*
######################################################################
#
# ...Check input parameters
#
    #...For help
    #
if [ "z${help}" = "zyes" -o $# = 0 ]; then
    help
    exit 1
fi
    #
    # ...For list of objects
    #
if [ "z${std}" = "z" -a ! -s "${std}" ]; then
    if [ "z${obj}" = "z" -o ! -s "${obj}" ]; then
        echo ""
        echo "***** LONG_red.sh: no objects were specified *****"
        echo ""
        help
        exit 1
    fi
fi
    #
    # ...Existence of input files
    #
for i in `cat ${obj} ${arc} ${std} ${std_arc}`
do
    if [ ! -f "${i}" ]; then
        echo "***** ############################################# *****"
        echo "***** NO file "${i}" on the disk...                 *****"
        echo "***** ############################################# *****"
        exit 10
    fi
done
######################################################################
#
# ...Some initial setups for IRAF
#
rm -f deletions.db logfile uparmimlhedit.par
rm -rf pyraf
if [ "z${IRAF}" = "zpyraf" ]; then
    echo "**** Will use pyraf with global login.cl ..."
else
    if [ ! -f login.cl ]; then
#       LONG_mkiraf.csh
        mkiraf
    fi
fi
 #
 # ...Some initial checks
 #
if [ -d uparm -o -f uparm ]; then
    rm -rf uparm
fi
cp -rpd ${St_red_dir}/uparm ./
    #
    #...With new idea I will copy to the local database ONLY
    #   those files I need, but not everything from the global one
    #
if [ -d database -o -f database ]; then
    rm -rf database
fi
if [ ! -d database ]; then
    mkdir database
fi
    #
if [ -f loginuser.cl ]; then rm -f loginuser.cl; fi
######################################################################
#
# ...Use our lists for one setup
#
if [ "z${obj}" = "z" -a  "z${arc}" = "z" ]; then
        #
        #...In case we have only SPST standard
        #   we are trying to reduce it
        #
    LONG_One_setup.sh obj=${std} arc=${std_arc}
    if [ $? != 0 ] ; then
        exit 1
    fi
        #
    sed 's/.fits/ws.fits/g'  <${std} >${std}a
    LONG_Std_red.sh obj=${std}a
    if [ $? != 0 ] ; then
        exit 1
    fi
else
        #    In case we objects:
        # ...1.Reduce all objects of similar setup
        #
    LONG_One_setup.sh obj=${obj} arc=${arc}
    if [ $? != 0 ] ; then
        exit 1
    fi
        #
        # ...2.Reduce standards for this setup
        #
    if [ "z${std}" = "z" -o ! -s "${std}" ]; then
        echo ""
        echo "***** LONG_red.sh: no standards were specified *****"
        echo ""
    else
        LONG_One_setup.sh obj=${std} arc=${std_arc}
        if [ $? != 0 ] ; then
            exit 1
        fi
        sed 's/.fits/ws.fits/g'  <${std} >${std}a
        LONG_Std_red.sh obj=${std}a
        if [ $? != 0 ] ; then
            exit 1
        fi
    fi
            #
            # ...3.Make flux calibration for this setup
            #
    sed 's/.fits/ws.fits/g'  <${obj} >${obj}a
    obj_setup="`cat ${obj}|head -1|sed 's/.fits//g'`"
    setup=`LONG_Make_setup.sh ${obj_setup}.fits std`
    closest=`LONG_Find_nearest.sh ${setup} std`
    if [ "z${closest}" != "zNONE" ]; then
        cp ${St_SPST}/${System}/${closest} ./
        LONG_flux.sh obj=${obj}a sens_file=${closest}
    else
        LONG_flux.sh obj=${obj}a
    fi
fi
#
#...Delete all temporary files
#
rm -f ${std}a ${obj}a
######################################################################
