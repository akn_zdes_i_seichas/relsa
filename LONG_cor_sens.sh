#!/bin/bash
#
############################################################################
#.COPYRIGHT    (c)  2006  SAAO, CapeTown
#.IDENT        LONG_cor_sens.sh
#.LANGUAGE     SHyRAF  :-)
#.AUTHOR       AKN (Kniazev A.Y.), SALT
#
#.KEYWORDS
#.PURPOSE      Try to exclude data for gaps from the sensitive curve file
#
#.COMMENT
#.VERSION      $Date: 2006/11/15 22:04:21 $
#              $Revision: 1.2 $
############################################################################
#
if [ -f ./LONG_epar.sh ]; then
    . ./LONG_epar.sh
else
    . LONG_epar.sh
fi
fits_file=$1
sens_file=$2
	#
	#...Main body
	#
if [ ${System} = "RSS" ]; then
    if [ -f ${file} ]; then
        numbers=`LONG_init_wav.sh ${fits_file} no`
        blue_end=`echo ${numbers}|awk '{print $3}'`
        cent_beg=`echo ${numbers}|awk '{print $4}'`
        cent_end=`echo ${numbers}|awk '{print $5}'`
        red_beg=` echo ${numbers}|awk '{print $6}'`
        awk "{if ( \$1 < ${blue_end} || \$1 > ${cent_beg} ) print \$0}" ${sens_file} >${sens_file}$$
        awk "{if ( \$1 < ${cent_end} || \$1 > ${red_beg} ) print \$0}" ${sens_file}$$ >${sens_file}
        rm ${sens_file}$$
    else
        echo "***** $0: No file ${file} *****"
        exit 1
    fi
else
    cp ${sens_file} /dev/null
fi
