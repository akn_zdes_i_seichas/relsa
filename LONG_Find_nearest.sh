#!/bin/bash
#
############################################################################
#.COPYRIGHT    (c)  2020  SAAO, CapeTown
#.IDENT        LONG_Find_nearest.sh
#.LANGUAGE     SHyRAF  :-)
#.AUTHOR       AKN   (Kniazev A.Y.), SALT
#
#.KEYWORDS
#.PURPOSE      Find the nearest file to the current one
#
#.COMMENT
#.VERSION      $Date: 2020/05/24$
#              $Revision: 1.00 $
############################################################################
#
. ./LONG_epar.sh
file=$1
std=$2
    #
    #...For RSS
    #
if [ "${System}" = "RSS" ]; then
        #
        #...Split input name and take JD time from there
        #
    LAMPID=`echo $file | awk -F \_ '{print $1}'`
    GRATING=`echo $file | awk -F \_ '{print $2}'`
    GRANGLE=`echo $file | awk -F \_ '{print $3}'`
    BIN=`echo $file | awk -F \_ '{print $5}'`
    JD=`echo $file | awk -F \_ '{print $6}'`
    if [ "z${std}" = "zobjshort" ]; then
	ls -1 ${St_database}/${System}/|fgrep fc|fgrep ${GRATING}|\
	   fgrep ${GRANGLE}|fgrep ${BIN} >List.txt
    elif [ "z${std}" = "zstd" ]; then
	ls -1 ${St_SPST}/${System}/|fgrep ${GRATING}|\
	   fgrep ${GRANGLE} >List.txt
    else
	ls -1 ${St_database}/${System}/|fgrep id|fgrep ${LAMPID}|fgrep ${GRATING}|\
	   fgrep ${GRANGLE}|fgrep ${BIN} >List.txt
    fi
#    ls /home/akniazev/Data/SALT_data/0ALL_GAINS |fgrep FAINT|sort -n -r -k 25,37>List.txt
    if [ -s List.txt ]; then
        best_name=$(cat List.txt|rpl '.tbl' ''|\
            #
            #...Format %s\\n\" in awk is EXTREMELY important!!! No spaces!!!
            #
        awk -F \_ "{printf \"%13.5f %13.5f %s\\n\", \$6, ${JD}, \$0}" |\
        awk 'BEGIN {dmax=100000.; name=NONE}
                   {time=$1; jd=$2;
                    dif=sqrt((time-jd)**2);
                    if (dif < dmax){dmax=dif; name=$3}
                   }
             END   {print name}')
    else
        result=NONE
    fi
        #
        #...Forming of output
        #
    if [ "z${result}" = "zNONE" ]; then
        echo -n "NONE"
    else
	if [ "z${std}" = "zobj" -o "z${std}" = "zstd" ]; then
            echo -n ${best_name}
	elif [ "z${std}" = "zobjshort" ]; then
            echo -n ${best_name}
        else
            echo -n "Std_${GRATING}_${ARANGLE}_${GRANGLE}_${BIN}"|sed 's/ //g'
        fi
    fi
    #
    #...For SCORPIO
    #
elif [ "${Sy#stem}" = "SCORPIO" ]; then
        #
    echo -n "NONE_NONE_NONE_NONE_NONE"
        #
    #
    #...For GSPEC
    #
elif [ "${System}" = "GSPEC" ]; then
        #
    echo -n "NONE_NONE_NONE_NONE_NONE"
        #
else
        #
        #...Forming of output for common case
        #
    echo -n "NONE_NONE_NONE_NONE_NONE"
fi
