#!/bin/sh
name=$1
nameA=${name}A
awk 'BEGIN{count=0}{if (/#/){count++;}if (count>1){print $0}}' ${name} >${nameA}
mv -v ${nameA} ${name}
