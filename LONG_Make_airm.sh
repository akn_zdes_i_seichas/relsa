#!/bin/bash
#
############################################################################
#.COPYRIGHT    (c)  2006  SAAO, CapeTown
#.IDENT        LONG_Make_airm.sh
#.LANGUAGE     SHyRAF  :-)
#.AUTHOR       AKN   (Kniazev A.Y.), SALT
#
#.KEYWORDS
#.PURPOSE      Calculation of airmass
#
#.COMMENT
#.VERSION      $Date: 2008/04/28 11:18:26 $
#              $Revision: 1.6 $
############################################################################
#
file=$1
file1=$2
Observatory=$3
iraffile=$4
    #
    #...First, calculate airmass
    #   Second, generate IRAF command
    #
if [ -f ${file} ]; then
    if [ "${System}" = "RSS" ]; then
	    #
	    #...First
	    #
	airmass=`LONG_read_des.sh ${file} TELALT|sed 's/ //g'|awk '{printf "%6.3f",$1}'`
#        airmass=`LONG_read_des.sh ${file} TELAZ|sed 's/ //g'|awk '{printf "%6.3f",$1}'`
	airmass=`echo "1"|awk "{print 1/cos(0.017453292*(90-${airmass}))}"|tr -d '\r'`
	    #
	    #...Second
	    #
	echo hedit images=${file1} fields=AIRMASS value=${airmass} add=yes addonly=yes ver-       >> $iraffile
	echo hedit images=${file1} fields=OBSERVAT value=${Observatory} add=yes addonly=yes ver-  >> $iraffile
	#
    elif [ "${System}" = "SCORPIO" ]; then
	    #
	    #...First
	    #
	airmass=`LONG_read_des.sh ${file} Z|sed 's/ //g'|awk '{printf "%6.3f",$1}'`
	airmass=`echo "1"|awk "{print 1/cos(0.017453292*(${airmass}))}"|tr -d '\r'`
	    #
	    #...Second
	    #
	echo hedit images=${file1} fields=AIRMASS value=${airmass} add=yes addonly=yes ver-       >> $iraffile
	#
	#...If System is unknown
	#
	#
    elif [ "${System}" = "GSPEC" ]; then
	    #
	    #...First
	    #
	echo hedit images=${file1} fields=OBSERVAT value=${Observatory} add=yes addonly=yes ver-  >> $iraffile
	echo setairmass images=${file1} update+ override+       >> $iraffile
	#
	#...If System is unknown
	#
    else
	echo hedit images=${file1} fields=AIRMASS value=1.0 add=yes addonly=yes ver-       >> $iraffile
    fi
else
    echo ""
    echo "***** LONG_Make_airm.sh: Input file {file} does not exist *****"
    echo ""
    exit 1
fi
