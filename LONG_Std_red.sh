#!/bin/bash
#
############################################################################
#.COPYRIGHT    (c)  2006  SAAO, CapeTown
#.IDENT        LONG_Std_red.sh
#.LANGUAGE     SHyRAF  :-)
#.AUTHOR       AKN   (Kniazev A.Y.), SALT
#
#.KEYWORDS     Long
#.PURPOSE      Script for reduction of standards and making sensitivity function
#
#.COMMENT      What script is doing:
#
#
#.VERSION      $Date: 2008/04/28 11:18:26 $
#              $Revision: 1.20 $
############################################################################
#
	 #
	 # ...Help
	 #
help() {
	echo 'Usage: LONG_Std_red.sh obj=std_list [sens_app=yes|no] [sens=yes|no [copy=yes|no]]'
	echo "where:"
	echo "       std_list - list of standards"
	echo '       sens_app=yes|no - append new star to the file with standard stars or create new'
	echo '       sens=yes|no     - make or not sensitivity curve finally'
	echo '       copy=yes|no     - copy or not sensitivity curve into the database directory'
	echo "      "
	echo "Program suggests: "
	echo "1. All objects were 2D-linearized and background was subtracted."
	echo '2. Each file in the input list has .fits extension'
	echo "      "
}
############################################################################
#
# ...Some initial setups
#
    #...Global
    #
if [ -f ./LONG_epar.sh ]; then
    . ./LONG_epar.sh
else
    echo " No LONG_epar.sh file in the current directory *****"
    exit 1
fi
    #
    #...Local
    #
eval $*
############################################################################
#
# ...Check input parameters
#
    #...For help
    #
if [ "z${help}" = "zyes" -o $# = 0 ]; then
    help
    exit 1
fi
    #
    # ...For list of objects
    #
if [ "z${obj}" = "z" -o ! -s "${obj}" ]; then
    echo ""
    echo "***** LONG_Std_red.sh: no standards were specified *****"
    echo ""
    help
    exit 1
fi
    #
    # ...Existence of input files
    #
for i in `cat ${obj}`
do
    if [ ! -f "${i}" ]; then
        echo "***** ############################################# *****"
        echo "***** NO file "${i}" on the disk...                 *****"
        echo "***** ############################################# *****"
        exit 10
    fi
done
############################################################################
	#
	# ...Some initial setup for IRAF
	#
if [ "z${IRAF}" = "zpyraf" ]; then
    echo "**** Will use pyraf with global login.cl ..."
else
    if [ ! -f login.cl ]; then
#       LONG_mkiraf.csh
        mkiraf
    fi
fi
if [ ! -d uparm ]; then mkdir uparm; fi
if [ ! -d database ]; then mkdir database; fi
#
if [ -f loginuser.cl ]; then rm -f loginuser.cl; fi
############################################################################
#
# ...Create output names
#
    #...For the standard
    #
file=`cat ${obj}|head -1`
std_out=`LONG_Make_setup.sh ${file} std`
if [ ${sens_app} = "no" ]; then
    rm -f ${std_out}
fi
#
# ...make iraf input file
#
iraffile_st=${iraffile}
iraffile="loginuser.cl"
echo print "'***** LONG_Std_red subroutine *****'"  >  $iraffile
echo print "'***** Create ascii file       *****'" >>  $iraffile
echo 'set imtype          = "fits"'                >> $iraffile
echo noao                                          >> $iraffile
echo twodspec                                      >> $iraffile
echo apextract dispaxis=1                          >> $iraffile
#
# ...Main loop
#
for i in `cat ${obj}`
do
    aa=`echo ${i}|sed 's/.fits/1d/g'`
    bb=`echo ${i}|sed 's/.fits/.0001.fits/g'`
    cc=`echo ${i}|sed 's/.fits//g'`
        #
        #...Create name for input file with data for standard star
        #
    star_name=`LONG_read_des.sh ${i} OBJECT|sed 's/ //g'|tr '-' '_'|tr '[:upper:]' '[:lower:]'`
        #
    if [ ! -f ${caldir}/${star_name}.dat ]; then
        echo ""
        echo "***** LONG_Std_red.sh: no file ${star_name}.dat for this star *****"
        echo "***** LONG_Std_red.sh: in the directory ${caldir} *****"
        echo ""
        rm -rf ${iraffile}
        exit 1
    fi
        #
        #
        #...Continue to make iraf file
        #
    echo imdel ${bb},${aa}.fits                     >> $iraffile
    echo apall input=${i} format=${format} interactive=${interactive_apall} nfind=${nfind} llimit=${llimit} ulimit=${ulimit} t_order=${t_order} ylevel=${ylevel} line=${line} >> $iraffile
    if [ "z${IRAF}" != "zpyraf" ]; then
        echo ${cc}                                      >> $iraffile
    fi
    echo rename ${bb} ${aa}.fits                    >> $iraffile
    echo longslit                                   >> $iraffile
    LONG_Make_airm.sh ${i} ${aa}.fits ${Observatory} $iraffile
    if [ $? != 0 ] ; then
        exit 1
    fi
    echo standard input=${aa}.fits output=${std_out} caldir=${caldir} interact=${interact_st} star_name=${star_name} extinct=${extinct} >> $iraffile
done
#
if [ ${sens} = "yes" ]; then
    echo imdel ${std_out}.fits >> $iraffile
    echo '! LONG_cor_sens.sh' ${aa}.fits ${std_out} >> $iraffile
    echo sensfunc standard=${std_out} sensitiv=${std_out}.fits interactive=${interactive_sens}  >> $iraffile
fi
#echo logout                                       >> $iraffile
#
#    echo '! kill -9 `ps aux|fgrep ${USER}|fgrep python|fgrep -v fgrep|awk "{print \$2}"`'    >> $iraffile
#
if [ "z${IRAF}" = "zpyraf" ]; then
#    mv ${iraffile} loginuser.cl
#    iraffile="loginuser.cl"
    pyraf -n
else
    xgterm -e "cl"
fi
############################################################################
	#
	#...Next iraf to calibrate standards itself
	#
if [ ${sens} = "yes" ]; then
	rm -rf ${iraffile}
	iraffile=${iraffile_st}
	echo print "'***** LONG_Std_red subroutine *****'"     > $iraffile
	echo print "'***** Calibrate standards itself *****'" >> $iraffile
	echo 'set imtype          = "fits"'                   >> $iraffile
# 	echo reset stdimage        = imt6                     >> $iraffile
# 	echo images                                           >> $iraffile
# 	echo plot                                             >> $iraffile
# 	echo dataio                                           >> $iraffile
# 	echo lists                                            >> $iraffile
# 	echo tv                                               >> $iraffile
# 	echo utilities                                        >> $iraffile
	echo noao                                             >> $iraffile
	echo twodspec                                         >> $iraffile
	echo longslit                                         >> $iraffile
	for i in `cat ${obj}`
	do
	    in_cal=`echo ${i}|sed 's/.fits/1d.fits/g'`
	    out_cal=`echo ${in_cal}|sed 's/1d/1d'${calibflux}'/g'`
	    echo imdel ${out_cal} >> $iraffile
	    echo calibrate input=${in_cal} output=${out_cal} sensitivity=${std_out}.fits extinction=${extinct} >> $iraffile
	done
	echo logout                                          >> $iraffile
	    #
    if [ -f login.cl ]; then
		    mv login.cl login.cl.ST
    fi
    cl < ${iraffile}
    if [ -f login.cl.ST ]; then
        mv login.cl.ST login.cl
    fi
	    #
	    #...Copy all files into Database directory
	    #
	if [ "${copy}" = "yes" ]; then
	    cp -v ${std_out}.fits ${St_SPST}/${System}/
	fi
fi
############################################################################
	#
	#...Delete all temporary files
	#
    rm -rf ${iraffile}
############################################################################
